let collection = [];

// Write the queue functions below.

function print() {
    // It will show the array

    return collection;
}

function enqueue(element) {
    //In this funtion you are going to make an algo that will add an element to the array
    // Mimic the function of push method
    let index = size();
    collection[index] = element;
    return collection;
}

function dequeue() {
    // In here you are going to remove the last element in the array
    let dequeueArr = [];

    for (let i = 1; i < size(); i++) {
        dequeueArr[i - 1] = collection[i];
    }

    collection = dequeueArr;
    return collection;
}

function front() {
    // In here, you are going to remove the first element
    return collection[0];
}

// starting from here, di na pwede gumamit ng .length property
function size() {
    let arrayLength = 0;

    for (let i = 0; i <= arrayLength; i++) {
        if (collection[i]) {
            arrayLength++;
        }
    }

    return arrayLength;
}

function isEmpty() {
    //it will check whether the function is empty or not
    if (collection[0]) {
        return false;
    } else {
        return true;
    }
}

module.exports = {
    collection,
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty,
};
